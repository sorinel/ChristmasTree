package ro.ansoft.christmastree;

import java.io.IOException;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Movie;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class GIFView extends View {
	private Movie mMovie;
	private long mMoviestart;
	private int mGifResource;

	public GIFView(Context context, int res) throws IOException {
		super(context);
		mGifResource = res;
		mMovie = Movie.decodeStream(getResources().openRawResource(mGifResource));
	}

	public GIFView(Context context, AttributeSet attrs) throws IOException {
		super(context, attrs);
		mMovie = Movie.decodeStream(getResources().openRawResource(mGifResource));
	}

	public GIFView(Context context, AttributeSet attrs, int defStyle)throws IOException {
		super(context, attrs, defStyle);
		mMovie = Movie.decodeStream(getResources().openRawResource(mGifResource));
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		long now = android.os.SystemClock.uptimeMillis();
		Paint p = new Paint();
		p.setAntiAlias(true);
		if (mMoviestart == 0) {
			mMoviestart = now;
		}
		int relTime;
		relTime = (int) ((now - mMoviestart) % mMovie.duration());
		mMovie.setTime(relTime);
		mMovie.draw(canvas, 0, 0);
		this.invalidate();
	}

	public int getImageResourceId() {
		return mGifResource;
	}

}
