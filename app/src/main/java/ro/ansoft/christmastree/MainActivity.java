package ro.ansoft.christmastree;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.ClipData;
import android.content.ContentValues;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.util.Log;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class MainActivity extends Activity { 

	private GridView mBaublesGrid;
	private static RelativeLayout mDropTargetTreeLayout;
	private static RelativeLayout mDropTargetBackgroundLayout;
	private ChristmasBaubleAdapter mBaublesAdapter;
	private ImageView mReindeerImageView;

	private Point deviceDimensions;
	private MediaPlayer mMediaPlayer;
	private Timer mMusicTimer;
	private TimerTask mMusicTimerTask;
	private Timer mReindeerTimer;
	private TimerTask mReindeerTimerTask;

	private static final int MAX_NUMBER_OF_DECORATIONS = 150;
	private static int sCurrentNumberOfDecorations;
	private static final String TAG = "ChristmasTree";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mReindeerImageView = (ImageView) findViewById(R.id.raindeer_img);
		mDropTargetTreeLayout = (RelativeLayout) findViewById(R.id.drop_target_tree);
		mDropTargetBackgroundLayout = (RelativeLayout) findViewById(R.id.drop_target_background);
		mBaublesGrid = (GridView) findViewById(R.id.baubles_grid);
		mBaublesAdapter = new ChristmasBaubleAdapter(this);
		mBaublesGrid.setAdapter(mBaublesAdapter);

		deviceDimensions = new Point();
		this.getWindowManager().getDefaultDisplay().getSize(deviceDimensions);
		Log.d(TAG, "Device height : " + deviceDimensions.y);

		mDropTargetTreeLayout.setOnDragListener(new OnDragListener() {
			@Override
			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					// do nothing
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					// do nothing
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					// do nothing
					break;
				case DragEvent.ACTION_DROP:
					Log.d(TAG, "Grid height: " + mBaublesGrid.getHeight());
					Log.d(TAG, "Allowed drop : " + (deviceDimensions.y - mBaublesGrid.getHeight()));
					Log.d(TAG, "Drop y: " + event.getY());

					if(event.getLocalState() == null) {
						break;
					}

					int[] gridLocation = new int[2];
					mBaublesGrid.getLocationOnScreen(gridLocation);
					Log.d(TAG, "Grid y: " + gridLocation[1]);

					if(event.getY() > (gridLocation[1] - 150)) {
						// dropped too low, do nothing
						((View) event.getLocalState()).setVisibility(View.VISIBLE);
						break;
					}

					if(sCurrentNumberOfDecorations >= MAX_NUMBER_OF_DECORATIONS) {
						showMaxNumberOfDecorationsDialog();
						break;
					}

					View originalImageView = (View) event.getLocalState();

					RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(originalImageView.getWidth(), originalImageView.getHeight());
					layoutParams.leftMargin = Math.round(event.getX()) - (originalImageView.getWidth() / 2);
					layoutParams.topMargin = Math.round(event.getY()) - (originalImageView.getHeight() / 2);

					View copiedImageView = null;

					if(originalImageView instanceof ImageView) {
						copiedImageView = new ImageView(MainActivity.this);
						((ImageView)copiedImageView).setImageDrawable(((ImageView)originalImageView).getDrawable());
						copiedImageView.setLayoutParams(layoutParams);
						((ImageView)copiedImageView).setScaleType(ImageView.ScaleType.CENTER_CROP);
						copiedImageView.setPadding(8, 8, 8, 8);
					} else if(originalImageView instanceof GIFView) {
						try {
							copiedImageView = new GIFView(MainActivity.this, ((GIFView) originalImageView).getImageResourceId());
							copiedImageView.setLayoutParams(layoutParams);
							copiedImageView.setPadding(8, 8, 8, 8);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}

					copiedImageView.setOnTouchListener(new OnTouchListener() {
						@Override
						public boolean onTouch(View view, MotionEvent event) {
							ClipData data = ClipData.newPlainText("", "");
							DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
							view.startDrag(data, shadowBuilder, view, 0);
							mDropTargetTreeLayout.removeView(view);
							return true;
						}
					});

					mDropTargetTreeLayout.addView(copiedImageView);
					originalImageView.setVisibility(View.VISIBLE);
					sCurrentNumberOfDecorations++;
					break;
				case DragEvent.ACTION_DRAG_ENDED:
					Log.d(TAG, "Drag ended");
					if(event.getLocalState() == null) {
						break;
					}
					((View) event.getLocalState()).setVisibility(View.VISIBLE);
					break;
				default:
					break;
				}
				return true;
			}
		});
	}

	@Override
	protected void onStart() {
		super.onStart();

		int carolTimerPeriod = 135000;
		mMusicTimer = new Timer();
		mMusicTimerTask = new TimerTask() {
			@Override
			public void run() {
				mMediaPlayer = MediaPlayer.create(MainActivity.this, R.raw.carol);
				mMediaPlayer.start();
			}
		};
		mMusicTimer.schedule(mMusicTimerTask, new Date(), carolTimerPeriod);

		startRaindeerAnimantion();
	}

	private void startRaindeerAnimantion() {
		int reindeerTimerPeriod = 10000;
		final int raindeerWidth = 240;
		final int raindeerHeight = 256;
		mReindeerTimer = new Timer();
		mReindeerTimerTask = new TimerTask() {
			@Override
			public void run() {
				MainActivity.this.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						final boolean isLeftSided = new Random().nextBoolean();
						RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(raindeerWidth, raindeerHeight);
						layoutParams.topMargin = new Random().nextInt(deviceDimensions.y - (deviceDimensions.y / 4));
						if(isLeftSided) {
							layoutParams.leftMargin = 0;
							mReindeerImageView.setImageResource(R.drawable.ic_reindeer_left);
							layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
						} else {
							layoutParams.rightMargin = 0;
							layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
							mReindeerImageView.setImageResource(R.drawable.ic_reindeer_right);
						}

						mReindeerImageView.setVisibility(View.VISIBLE);
						mReindeerImageView.setLayoutParams(layoutParams);
						Animation myFadeInAnimation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.fadein);
						mReindeerImageView.startAnimation(myFadeInAnimation);
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								mReindeerImageView.setVisibility(View.GONE);
							}
						}, 2500);
					}
				});
			}
		};
		mReindeerTimer.schedule(mReindeerTimerTask, new Date(), reindeerTimerPeriod);
	}

	@Override
	protected void onStop() {
		super.onStop();

		mReindeerTimer.cancel();
		mMusicTimer.cancel();
		try {
			if(mMediaPlayer != null && mMediaPlayer.isPlaying()) {
				mMediaPlayer.stop();
			}
		} catch (IllegalStateException e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void onDestroy() {
	    super.onDestroy();
	    sCurrentNumberOfDecorations = 0;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case R.id.action_reset:
			mDropTargetTreeLayout.removeAllViews();
			mBaublesAdapter.notifyDataSetChanged();
			sCurrentNumberOfDecorations = 0;
			break;
		case R.id.action_screenshot:
			takeScreenShot(mDropTargetBackgroundLayout);
			break;
		case R.id.action_change_background:
			ChooserDialogFragment dialogBackground = new ChooserDialogFragment();
			Bundle argsBackground = new Bundle();
			argsBackground.putString(ChooserDialogFragment.CHOOSE, ChooserDialogFragment.CHOOSE_BACKGROUND);
			dialogBackground.setArguments(argsBackground);
			dialogBackground.show(getFragmentManager(), "");
			break;
		case R.id.action_change_tree:
			ChooserDialogFragment dialogTree = new ChooserDialogFragment();
			Bundle argsTree = new Bundle();
			argsTree.putString(ChooserDialogFragment.CHOOSE, ChooserDialogFragment.CHOOSE_TREE);
			dialogTree.setArguments(argsTree);
			dialogTree.show(getFragmentManager(), "");
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void takeScreenShot(View view) {
		String year = "" + Calendar.getInstance().get(Calendar.YEAR);
		String month = "" + Calendar.getInstance().get(Calendar.MONTH);
		String day = "" + Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
		String hour = "" + Calendar.getInstance().get(Calendar.HOUR);
		String minute = "" + Calendar.getInstance().get(Calendar.MINUTE);
		String second = "" + Calendar.getInstance().get(Calendar.SECOND);

		String pictureName = new StringBuilder().append("ChristmasTree_")
				.append(year)
				.append(month)
				.append(day)
				.append("_")
				.append(hour)
				.append(minute)
				.append(second)
				.append(".jpg")
				.toString();

		String localStoragePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath() + "/" + pictureName;
		File imageFile;
		Bitmap bitmap;

		view.setDrawingCacheEnabled(true);
		bitmap = Bitmap.createBitmap(view.getDrawingCache());
		view.setDrawingCacheEnabled(false);

		OutputStream fileOutputStream = null;
		imageFile = new File(localStoragePath);

		try {
		    fileOutputStream = new FileOutputStream(imageFile);
		    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fileOutputStream);
		    fileOutputStream.flush();
		    fileOutputStream.close();

		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		    Toast.makeText(this, "Failed to take screenshot", Toast.LENGTH_SHORT).show();
		} catch (IOException e) {
		    e.printStackTrace();
		    Toast.makeText(this, "Failed to take screenshot", Toast.LENGTH_SHORT).show();
		}

		Toast.makeText(this, "Screenshot taken", Toast.LENGTH_SHORT).show();
		ContentValues values = new ContentValues();

	    values.put(Images.Media.DATE_TAKEN, System.currentTimeMillis());
	    values.put(Images.Media.MIME_TYPE, "image/jpeg");
	    values.put(MediaStore.MediaColumns.DATA, localStoragePath);

	    getContentResolver().insert(Images.Media.EXTERNAL_CONTENT_URI, values);
	}
	
	private void showMaxNumberOfDecorationsDialog() {
		Toast.makeText(MainActivity.this, "Ho! Ho! Ho! You have reached the maximum number of decorations!", Toast.LENGTH_LONG).show();
	}

	static void setBackgroundLayout(int resourceId) {
		mDropTargetBackgroundLayout.setBackgroundResource(resourceId);
	}
	
	static void setTreeLayout(int resourceId) {
		mDropTargetTreeLayout.setBackgroundResource(resourceId);
		mDropTargetTreeLayout.removeAllViews();
		sCurrentNumberOfDecorations = 0;
	}
}
