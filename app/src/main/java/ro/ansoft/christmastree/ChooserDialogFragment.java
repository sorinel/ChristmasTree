package ro.ansoft.christmastree;

import ro.ansoft.christmastree.R;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class ChooserDialogFragment extends DialogFragment {

	static final String CHOOSE = "choose";
	static final String CHOOSE_TREE = "chooseTree";
	static final String CHOOSE_BACKGROUND = "chooseBackground";
	private View mMainLayout;
	private AlertDialog mAlertDialog;

	public ChooserDialogFragment() {

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
    	String info = "";
    	LayoutInflater inflater = getActivity().getLayoutInflater();
    	mMainLayout = null;
    	Bundle args = getArguments();
    	if(args != null) {
    		if(CHOOSE_TREE.equals(args.getString(CHOOSE))) {
    			info = "Choose Christmas tree";
    			mMainLayout = inflater.inflate(R.layout.fragment_dialog_change_tree, null);
    			RadioGroup radioGroupTree = (RadioGroup) mMainLayout.findViewById(R.id.radio_group_tree);
    			radioGroupTree.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						switch(checkedId) {
						case R.id.radio_btn_tree_1:
							MainActivity.setTreeLayout(R.drawable.ic_tree_1);
							break;
						case R.id.radio_btn_tree_2:
							MainActivity.setTreeLayout(R.drawable.ic_tree_2);
							break;
						case R.id.radio_btn_tree_3:
							MainActivity.setTreeLayout(R.drawable.ic_tree_3);
							break;
						case R.id.radio_btn_tree_4:
							MainActivity.setTreeLayout(R.drawable.ic_tree_4);
							break;
						case R.id.radio_btn_tree_5:
							MainActivity.setTreeLayout(R.drawable.ic_tree_5);
							break;
						}
						mAlertDialog.dismiss();
					}
				});
    			
    		} else if(CHOOSE_BACKGROUND.equals(args.getString(CHOOSE))) {
    			info = "Choose background image";
    			mMainLayout = inflater.inflate(R.layout.fragment_dialog_change_background, null);
    			RadioGroup radioGroupBackground = (RadioGroup) mMainLayout.findViewById(R.id.radio_group_background);
    			radioGroupBackground.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						switch(checkedId) {
						case R.id.radio_btn_background_1:
							MainActivity.setBackgroundLayout(R.drawable.ic_background_1);
							break;
						case R.id.radio_btn_background_2:
							MainActivity.setBackgroundLayout(R.drawable.ic_background_2);
							break;
						case R.id.radio_btn_background_3:
							MainActivity.setBackgroundLayout(R.drawable.ic_background_3);
							break;
						case R.id.radio_btn_background_4:
							MainActivity.setBackgroundLayout(R.drawable.ic_background_4);
							break;
						case R.id.radio_btn_background_5:
							MainActivity.setBackgroundLayout(R.drawable.ic_background_5);
							break;
						case R.id.radio_btn_background_6:
							MainActivity.setBackgroundLayout(R.drawable.ic_background_6);
							break;
						case R.id.radio_btn_background_7:
							MainActivity.setBackgroundLayout(R.drawable.ic_background_7);
							break;
						case R.id.radio_btn_background_8:
							MainActivity.setBackgroundLayout(R.drawable.ic_background_8);
							break;
						case R.id.radio_btn_background_9:
							MainActivity.setBackgroundLayout(R.drawable.ic_background_9);
							break;
						case R.id.radio_btn_background_10:
							MainActivity.setBackgroundLayout(R.drawable.ic_background_10);
							break;
						case R.id.radio_btn_background_11:
							MainActivity.setBackgroundLayout(R.drawable.ic_background_11);
							break;
						}
						mAlertDialog.dismiss();
					}
				});

    		}
    	}

        mAlertDialog = new AlertDialog.Builder(getActivity())
        	.setView(mMainLayout)
            .setTitle(info)
            .create();

        return mAlertDialog;
	}

}
