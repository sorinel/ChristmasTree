package ro.ansoft.christmastree;

import java.io.IOException;

import android.content.ClipData;
import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class ChristmasBaubleAdapter extends BaseAdapter {

	private Context mContext;
	private static final String IMAGEVIEW_TAG = "bauble";

	private Integer[] mBaubles = {
			R.drawable.ic_bauble_1,
			R.drawable.ic_bauble_2,
			R.drawable.ic_bauble_3,
			R.drawable.ic_bauble_4,
			R.drawable.ic_bauble_5,
			R.drawable.ic_bauble_animat_3,
			R.drawable.ic_bauble_6,
			R.drawable.ic_ornament_1,
			R.drawable.ic_ornament_5,
			R.drawable.ic_ornament_6,
    };

	public ChristmasBaubleAdapter(Context context) {
		mContext = context;
	}

	@Override
    public int getCount() {
	    return mBaubles.length;
    }

	@Override
    public Object getItem(int arg0) {
	    return null;
    }

	@Override
    public long getItemId(int arg0) {
	    return 0;
    }

	@Override
    public View getView(int position, View convertView, final ViewGroup parent) {
		ImageView imageView = null;
		GIFView gifView = null;

		// check if gifImage
		if(mBaubles[position] == R.drawable.ic_bauble_animat_3) {
			try {
				gifView = new GIFView(mContext, mBaubles[position]);
			} catch(IOException e) {
				e.printStackTrace();
			}
			gifView.setLayoutParams(new GridView.LayoutParams(70, 70));
			gifView.setPadding(8, 8, 8, 8);

			gifView.setOnTouchListener(new OnTouchListener() {
				@Override
				public boolean onTouch(View view, MotionEvent event) {
					ClipData data = ClipData.newPlainText("", "");
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);
					return true;
				}
			});

			return gifView;
		} else {
			imageView = new ImageView(mContext);
			// instalation light
			if(mBaubles[position] == R.drawable.ic_bauble_6) {
				imageView.setLayoutParams(new GridView.LayoutParams(90, 70));
			} else {
				imageView.setLayoutParams(new GridView.LayoutParams(70, 70));
			}
			imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			imageView.setPadding(8, 8, 8, 8);

			imageView.setImageResource(mBaubles[position]);
			imageView.setTag(IMAGEVIEW_TAG);

			imageView.setOnTouchListener(new OnTouchListener() {
				@Override
				public boolean onTouch(View view, MotionEvent event) {
					ClipData data = ClipData.newPlainText("", "");
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);
					return true;
				}
			});

	        return imageView;
		}
    }
}
